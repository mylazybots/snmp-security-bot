SWITCHES_LIST=/tmp/switches.list
COMMUNITY_FILE=/tmp/community.list
 
for SW_NAME in $(cat $SWITCHES_LIST)
 
for COM in $(cat $COMMUNITY_FILE); DO
 
SNMP_TEST=$(/usr/bin/snmpwalk -v2c -t 3 -r 1 -c $COM $SW_NAME sysName 2>/dev/null | awk -F ":" '{ print $4}')
 
         if [ "$SNMP_TEST" != "" ]; then
                               /bin/echo "$SW_NAME,  Hit with string  $COM"
         else
                               /bin/echo "$SW_NAME, Not a hit with $COM"
         done
 
done